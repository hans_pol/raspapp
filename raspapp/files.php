<?php
header('Content-Type: application/json');

$dir          = "../files"; //path

$list = array(); //main array

if(is_dir($dir)){
    if($dh = opendir($dir)){
        while(($file = readdir($dh)) != false){

            if($file == "." or $file == ".."){
                //...
            } else { //create object with two fields
                $list3 = array(
                'name' => $file, 
                'path' => $_SERVER['DOCUMENT_ROOT'] .'/files/'. $file);
                array_push($list, $list3);
            }
        }
    }

    $return_array = array($list);

    echo json_encode($return_array);
}

?>
