var start_point_select = 1; //selected item on starup
var select_id;
var total_menu_items = 0; 
var dir_location = 'files.php'; 
var video_active = false;
var video_pause = false;
var new_session = true;
var welcome_screen = true;
	

$( document ).ready(function() {
    get_files();
});

// keyboard/remote press listner 
document.onkeydown = function(e) {
	console.log(e.which);	//key listner
    switch(e.which) {

        case 38: select_handler('min'); // up
        break;

        case 40: select_handler('plus'); // down
        break;
	
		case 13: fire_video(); // ok/enter
        break;
        
        case 179: pause_video();
		break;
		
		case 83: stop_video();
		break;
	
        default: return; // exit this handler for other keys
    }
    
	e.preventDefault(); // prevent the default action (scroll / move caret)
	
	startup_message();
};

//get files data form php trough json
function get_files(){
	
	$.getJSON( dir_location, function( result ) {
		output = JSON.stringify(result);
		obj = JSON.parse(output)
		return file_menu_builder(obj);
	});	
}

//start-up message
function startup_message(){
	if(welcome_screen == true){
		$( ".host_ip" ).hide();
		welcome_screen = false;
	}
}

//from int position to front-end values determination
function select_handler(id){
	
	if(typeof select_id == 'undefined'){
		select_id = start_point_select;
		$('#'+ select_id).addClass('selected');
	} else if (select_id > 1 && id == 'min'){
		$('#'+ select_id).removeClass('selected');
		select_id--;
		$('#'+ select_id).addClass('selected');
		console.log(select_id);
	} else if (select_id < total_menu_items && id == 'plus'){
		$('#'+ select_id).removeClass('selected');
		select_id++;
		$('#'+ select_id).addClass('selected');
		console.log(select_id);
	}
}

//start video and stops old stream when is active
function fire_video(){
	if(video_active == false & welcome_screen == false){
		console.log('video false');
		var vid_path = $("#target > #"+select_id).attr("file-path");
		if(vid_path){	
			var get_url = "http://localhost:9192/dplay?url="+vid_path+"&omxoptions=--win;300,140,1900,1040;stop";

			$.get( get_url, function( data ) {
				console.log('start_succes');
				video_active = true;
				return;
			});
			
		}
	} else {
		video_active = false;
		stop_video();
	}

}

function pause_video(){
	
		var get_url = "http://localhost:9192/omxcmd?cmd=pause";
			 $.ajax({
				type: 'GET',
				url: get_url,
				dataType: 'text',
				complete: function(data){
				   	console.log('pause_succes');

				}
		 });
		 
}

function stop_video(){
	
		var get_url = "http://localhost:9192/omxcmd?cmd=stop";
			 $.ajax({
				type: 'GET',
				url: get_url,
				dataType: 'text',
				complete: function(data){
				   	console.log('stop_succes');

				}
		 });
	
}

//loop creates select buttons for navigation
function file_menu_builder(obj){
	
	$.each(obj[0], function (index, file) {
		console.log(file.path);
		var target_item = $('#target').append($('<div/>', { id: index + 1, 'class' : 'item_optie', 'file-path' : file.path}));
			
		total_menu_items++;
		
		$( "#target > #"+total_menu_items ).prepend( '<span>'+ file.name + '</span>');
	});
	
	return select_handler();
}